from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator


# Create your models here.

class Inventory(models.Model):
    name = models.CharField(default="test", max_length=200, blank=True)
    gtin = models.CharField(default='0000000000000', max_length=13, validators=[RegexValidator(r'^\d{13,13}$')])
    expiry_date = models.DateField('expiry date')
    image_url = models.URLField(default='', editable=False)
    def __str__(self):
        return self.name

    def is_outdated(self):
        return self.expiry_date <= timezone.now().date()

    def get_expiry_date(self):
        return self.expiry_date
        
    def get_gtin(self):
        return self.gtin

    def get_image_url(self):
        return self.image_url
